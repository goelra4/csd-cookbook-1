node.override['java']['jdk_version'] = '7'

node.default['tomcat']['base_version'] = 7
include_recipe "mw-tomcat"

include_recipe "java"
include_recipe "mw-apache2.4"

case deploy_env
when nil, ''
  env='dev'
when 'release'
  env='prod'
when 'test'
  env='test'
when 'prod'
  env='prod'
when 'ci'
  env='dev'
else
  env='dev'
end


include_recipe "csd::ssl"
template "/etc/httpd/conf-enabled/csd.proxy.conf" do
  owner 'apache'
  group 'apache'
  mode 0644
  source 'csd.proxy.conf.erb'
  action :create
end

