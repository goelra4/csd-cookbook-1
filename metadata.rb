name             "csd"
maintainer       "Novartis Team"
maintainer_email "NX_MW_ORG@dl.mgd.novartis.com"
license          "All rights reserved"
description      "cds system"
version          "3.0.0"

depends "mw-tomcat"
depends "java"
depends "mw-apache2.4"

